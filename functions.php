<?php

if (!function_exists('simplicity_init_options')) {
    function simplicity_init_options() {
	$options['hide_search'] = FALSE;
	$options['banner_format'] = 'image';
	update_option('vs-simplicity-options', $options);
    }
}    
add_action('init', 'simplicity_init_options');	

?>